import React, { Component } from 'react'
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import { Link } from "react-router-dom";




// Button  component starts here
const styles = theme => ({
  firstBotton: {
    marginBottom:"25px",
    
  },

  mainGrid: {
    marginTop:"165px",
  }

 
});



// creating the class
const Home = ({classes}) => {
    return(
      //adding the JSX and adding style to it
        <Grid container alignItems='center' direction='column' className={classes.mainGrid}>
          
          <Grid item sm={6} xs={12} className={classes.firstBotton}>
          
          <Button variant="raised" color="primary" component={Link} to='/input'>
           Create Invoice
          </Button>
          </Grid>
          
          <Grid item sm={6} xs={12}> 
            <Button variant="raised" color="primary" component={Link} to='/input'>
           pay with phone number
            </Button>
          </Grid>
          
        </Grid>
        
    )
}



  

export default withStyles(styles)(Home) 
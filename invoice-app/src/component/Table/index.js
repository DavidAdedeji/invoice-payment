import React, { Component } from 'react'
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import { Link } from "react-router-dom";

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
  },
});

let id = 0;
function createData(description, quantity, unitPrice, Disc, taxExcludedPrices) {
  id += 1;
  return { id, description, quantity, unitPrice, Disc, taxExcludedPrices };
}

const data = [
  createData('Frozen yoghurt', "1.000 unit(s)", 1.79900, 24),
  createData('Ice cream sandwich',"1.000 unit(s)", 16.50, 37),
  createData('Eclair', "1.000 unit(s)", 750.00, 24),
  createData('Cupcake', "1.000 unit(s)", 3.7, 67),
  createData('Gingerbread', "1.000 unit(s)", 16.0, 49),
];

 function InvoiceTable(props) {
  const { classes } = props;

  return (

      <Table className={classes.table}>
        <TableHead>
          <TableRow>
            <TableCell>Description</TableCell>
            <TableCell numeric>Quantity</TableCell>
            <TableCell numeric>Unit price</TableCell>
            <TableCell numeric>Disc (%)</TableCell>
            <TableCell numeric>Action</TableCell>
           
          </TableRow>
        </TableHead>
        <TableBody>
          {data.map(n => {
            return (
              <TableRow key={n.id}>
        
                <TableCell component="th" scope="row">
                  {n.description}
                </TableCell>
                <TableCell numeric>{n.quantity}</TableCell>
                <TableCell numeric>{n.unitPrice}</TableCell>
                <TableCell numeric>{n.Disc}</TableCell>
                
                <TableCell numeric>
                <Button variant="raised" color="default" size="small" component={Link} to='/Invoice'>
                View
                </Button>
                </TableCell>
               
                
            
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
  
  );
}

export default withStyles(styles)(InvoiceTable)
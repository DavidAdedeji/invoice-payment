import React, { Component } from 'react'
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { Link } from "react-router-dom";

const CustomTableCell = withStyles(theme => ({
    head: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    body: {
      fontSize: 14,
    },
  }))(TableCell);
  
  const styles = theme => ({
    root: {
      width: '100%',
      marginTop: theme.spacing.unit * 3,
      overflowX: 'auto',
      // margin: 300,
    },
    table: {
      minWidth: 700,
    },
    row: {
      '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.background.default,
      },
    },
  });
  
  const styles2 = {
    card: {
      minWidth: 275,
      // marginTop: 300,
    },
    bullet: {
      display: 'inline-block',
      margin: '0 2px',
      transform: 'scale(0.8)',
    },
    title: {
      marginBottom: 16,
      fontSize: 14,
    },
  
  };
  
  let id = 0;
  function createData(name, quantity, unitPrice, price, taxExcludedPrices) {
    id += 1;
    return { id, name, quantity, unitPrice, price, taxExcludedPrices };
  }
  
  const data = [
    createData('Frozen yoghurt', 1000, 1000, 1000, 1000),
   
    
  ];
  
 function Invoice(props) {
    const { classes } = props;
  
    return (
      <Card className={classes.card}>
      <CardContent>
       <span>
       <h3>Invoice:011243254</h3>
       <Button variant="raised" color="default" size="small" component={Link} to='/PaymentMethod'>
                Pay </Button>
       </span>
      <Paper className={classes.root}>
        <Table className={classes.table}>
          <TableHead>
            <TableRow>
              <CustomTableCell>Name</CustomTableCell>
              <CustomTableCell numeric>Quantity</CustomTableCell>
              <CustomTableCell numeric>Unit Price</CustomTableCell>
              <CustomTableCell numeric> Price</CustomTableCell>
              <CustomTableCell numeric>Tax Excluded Prices</CustomTableCell>
              
            </TableRow>
          </TableHead>
          <TableBody>
            {data.map(n => {
              return (
                <TableRow className={classes.row} key={n.id}>
                  <CustomTableCell component="th" scope="row">
                    {n.name}
                  </CustomTableCell>
                  <CustomTableCell numeric>{n.quantity}</CustomTableCell>
                  <CustomTableCell numeric>{n.unitPrice}</CustomTableCell>
                  <CustomTableCell numeric>{n.price}</CustomTableCell>
                  <CustomTableCell numeric>{n.taxExcludedPrices}</CustomTableCell>
              
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </Paper>
      </CardContent>
      </Card>
    );
  }
  
  export default withStyles(styles)(Invoice)
import React, { Component } from 'react'
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
// import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import { Link } from "react-router-dom";


//invoice component begins

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  firstBotton: {
        marginBottom:"10px",
        
      },
  // menu: {
  //   width: 200,
  // },
 
});

const Input = ({classes}) => {
    return (
      <Grid container alignItems='center' direction='column' className={classes.container}>
          <Grid item sm={6} xs={12} className={classes.firstBotton}>
          
          <TextField
          id="number"
          label="Invoice"
          // value={this.state.age}
          // onChange={this.handleChange('age')}
          type="number"
          className={classes.textField}
          InputLabelProps={{
            shrink: true,
          }}
          margin="normal"
        />
          </Grid>

           <Grid item sm={6} xs={12}> 
           <Button variant="raised" size="small" color="primary" className={classes.firstBotton} component={Link} to='/InvoiceTable'>
            Submit
             </Button>
           </Grid>
      </Grid>
    );

}


export default withStyles(styles)(Input)
//invoice component ends
  
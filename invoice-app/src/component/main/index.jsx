import React, { Component } from 'react'
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Home from '../home';

import  Invoice from '../CardwithTable';
import Input from '../invoice';
import InvoiceTable from '../Table';
import PaymentMethod from '../Dialogs'
import SimpleModalWrapped from '../modal'
import { Route, Switch } from 'react-router' 


const styles = theme => ({
    root: {
      backgroundColor : "#fafafa",
      minHeight:'100vh',
      padding:"15px",
    
      
    },

    paper: {
      padding: theme.spacing.unit * 2,
      textAlign: 'center',
      color: theme.palette.text.secondary,
      
    },

  });

class Main extends Component {
    constructor(props) {
        super(props);
        this.state = {  };
    }
    render() {
        const {classes} = this.props
        return (
           <div className={classes.root}>
           <Switch>
               <Route path='/Home' exact render={props=><Home {...props}/>}/>
               <Route path='/Invoice' exact render={props=><Invoice {...props}/>}/>
               <Route path='/InvoiceTable' exact render={props=><InvoiceTable {...props}/>}/>
               <Route path='/Input' exact render={props=><Input {...props}/>}/>
               <Route path='/PaymentMethod' exact render={props=><PaymentMethod {...props}/>}/>
               <Route path='/SimpleModalWrapped' exact render={props=><SimpleModalWrapped {...props}/>}/>
           </Switch> 
           </div> 
        );
    }
}



// export default Main;
// you are exporting it with the styles
export default withStyles(styles)(Main);
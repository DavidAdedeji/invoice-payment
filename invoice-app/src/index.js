import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import Main from './component/main';
import {BrowserRouter as Router} from 'react-router-dom';
console.log(Main);

ReactDOM.render(
 
    <Router>
    <Main />
    </Router>,
 
document.getElementById('root'));
registerServiceWorker();
